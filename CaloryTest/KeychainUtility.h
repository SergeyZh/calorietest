//
//  KeychainUtility.h
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 15.06.15.
//
//

#import <Foundation/Foundation.h>

@interface KeychainUtility : NSObject

+ (NSString*)getPasswordForKey:(NSString*)aKey;
+ (void)setPassword:(NSString*)aPassword forKey:(NSString*)aKey;
+ (void)removePasswordForKey:(NSString*)aKey;

@end
