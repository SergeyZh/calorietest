//
//  Meal.h
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 19.06.15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "CalendarEntity.h"

@class Account;

@interface Meal : CalendarEntity

@property (nonatomic, retain) NSNumber * calorie;
@property (nonatomic, retain) NSString * title;
@property (nonatomic, retain) Account *account;

@end
