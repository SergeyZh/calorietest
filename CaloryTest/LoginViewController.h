//
//  CTLoginViewController.h
//  CaloryTest
//
//  Created by Sergey Zhitnikov on 13.06.15.
//
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface LoginViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
