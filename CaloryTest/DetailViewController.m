//
//  DetailViewController.m
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 15.06.15.
//
//

#import "DetailViewController.h"
#import <CoreData/CoreData.h>
#import <Parse/Parse.h>
#import "NSDate+DateCutter.h"

@interface DetailViewController ()

@property (weak, nonatomic) IBOutlet UITextField *mealTextField;
@property (weak, nonatomic) IBOutlet UITextField *caloriesTextField;
@property (weak, nonatomic) IBOutlet UIDatePicker *datePicker;

@end

@implementation DetailViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _datePicker.maximumDate = [NSDate date];
    
    if ([self.meal.title length]) {
        _mealTextField.text = self.meal.title;
        _caloriesTextField.text = [self.meal.calorie stringValue];
        _datePicker.date = [NSDate combineDate:self.meal.date withTime:self.meal.timeOfDay];
    }
}

// Hide keyboard on touch outside
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (IBAction)save:(UIBarButtonItem *)sender {
    if ([self passValidation])
    {
        _meal.title = _mealTextField.text;
        _meal.calorie = [NSNumber numberWithInteger:[_caloriesTextField.text integerValue]];
        _meal.date = [_datePicker.date day];
        _meal.timeOfDay = [_datePicker.date timeOfDay];
        _meal.account = (Account *)[self.managedObjectContext objectWithID:_userID];
        
        [self.delegate saveEntity:self add:!self.isEdit];
        
        [self.navigationController popViewControllerAnimated:YES];
    }
}

- (BOOL)passValidation
{
    NSMutableString *resultError = [NSMutableString string];
    
    if (!_mealTextField.text.length) {
        [resultError appendString:@"Введите блюдо"];
    }
    
    if (!_caloriesTextField.text.length) {
        if (resultError.length) {
            [resultError appendString:@"\n"];
        }
        [resultError appendString:@"Введите количество калорий"];
    }
    
    if (resultError.length) {
        UIAlertView *validationAlert = [[UIAlertView alloc] initWithTitle:nil
                                                                  message:resultError
                                                                 delegate:self
                                                        cancelButtonTitle:@"ОК"
                                                        otherButtonTitles:nil];
        [validationAlert show];
        
        return NO;
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
