//
//  NSDate+DateCutter.h
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 19.06.15.
//
//

#import <Foundation/Foundation.h>

@interface NSDate (DateCutter)

- (NSDate *)day;
- (NSDate *)timeOfDay;
+ (NSDate *)combineDate:(NSDate *)date withTime:(NSDate *)time;

@end
