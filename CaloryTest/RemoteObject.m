//
//  RemoteObject.m
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 20.06.15.
//
//

#import "RemoteObject.h"


@implementation RemoteObject

@dynamic createdAt;
@dynamic updatedAt;
@dynamic objectId;

@end
