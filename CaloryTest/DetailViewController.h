//
//  DetailViewController.h
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 15.06.15.
//
//

#import <UIKit/UIKit.h>
#import "Meal.h"

@protocol DetailViewControllerDelegate;

@interface DetailViewController : UIViewController

@property (nonatomic, weak) id<DetailViewControllerDelegate> delegate;
@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) Meal *meal;
@property (nonatomic, assign) BOOL isEdit;
@property (nonatomic, strong) NSManagedObjectID *userID;

@end

@protocol DetailViewControllerDelegate

- (void)saveEntity:(DetailViewController *)controller add:(BOOL)add;

@end
