//
//  KeychainUtility.m
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 15.06.15.
//
//

#import "KeychainUtility.h"
#import <Security/Security.h>

@interface KeychainUtility ()

+ (NSMutableDictionary*)dictionaryForKey:(NSString*)aKey;

@end

@implementation KeychainUtility

static const NSString *SERVICE_NAME = @"srgzhi.CalorieTest";

+ (NSMutableDictionary*)dictionaryForKey:(NSString*)aKey
{
    NSData *encodedKey = [aKey dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *searchDictionary = [NSMutableDictionary dictionary];
    
    [searchDictionary setObject:(__bridge id)kSecClassGenericPassword forKey:(__bridge id)kSecClass];
    [searchDictionary setObject:encodedKey forKey:(__bridge id)kSecAttrGeneric];
    [searchDictionary setObject:encodedKey forKey:(__bridge id)kSecAttrAccount];
    [searchDictionary setObject:SERVICE_NAME forKey:(__bridge id)kSecAttrService];
    
    return searchDictionary;
}

+ (NSString*)getPasswordForKey:(NSString*)aKey
{
    NSString *password = nil;
    
    NSMutableDictionary *searchDictionary = [self dictionaryForKey:aKey];
    [searchDictionary setObject:(__bridge id)kSecMatchLimitOne forKey:(__bridge id)kSecMatchLimit];
    [searchDictionary setObject:(id)kCFBooleanTrue forKey:(__bridge id)kSecReturnData];
    
    NSData *result = nil;
    CFTypeRef inTypeRef = (__bridge CFTypeRef)result;
    SecItemCopyMatching((__bridge CFDictionaryRef)searchDictionary,
                                          &inTypeRef);
    
    if (result)
    {
        password = [[NSString alloc] initWithData:result encoding:NSUTF8StringEncoding];
    }
    return password;
}

+ (void)removePasswordForKey:(NSString*)aKey
{
    NSMutableDictionary *keyDictionary = [self dictionaryForKey:aKey];
    SecItemDelete((__bridge CFDictionaryRef)keyDictionary);
}

+ (void)setPassword:(NSString*)aPassword forKey:(NSString*)aKey
{
    [KeychainUtility removePasswordForKey:aKey];
    
    NSData *encodedPassword = [aPassword dataUsingEncoding:NSUTF8StringEncoding];
    
    NSMutableDictionary *keyDictionary = [self dictionaryForKey:aKey];
    [keyDictionary setObject:encodedPassword forKey:(__bridge id)kSecValueData];
    SecItemAdd((__bridge CFDictionaryRef)keyDictionary, nil);
}

@end
