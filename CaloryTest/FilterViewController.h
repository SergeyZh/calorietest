//
//  FilterViewController.h
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 18.06.15.
//
//

#import <UIKit/UIKit.h>

@protocol FilterDelegate <NSObject>

- (void)filterFrom:(NSDate *)from to:(NSDate *)to;
- (void)clearFilter;

@end

@interface FilterViewController : UIViewController <UIBarPositioningDelegate>

@property (weak, nonatomic) id<FilterDelegate> delegate;
@property (strong, nonatomic) NSDate *fromDate;
@property (strong, nonatomic) NSDate *toDate;

@end
