//
//  Account.m
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 15.06.15.
//
//

#import "Account.h"
#import "Meal.h"
#import "KeychainUtility.h"


@implementation Account

- (NSString*)password
{
    if (self.username)
        return [KeychainUtility getPasswordForKey:self.username];
    return nil;
}

- (void)setPassword:(NSString*)aPassword
{
    if (self.username)
        [KeychainUtility setPassword:aPassword forKey:self.username];
}

- (void)prepareForDeletion
{
    if (self.username)
        [KeychainUtility removePasswordForKey:self.username];
}

@end
