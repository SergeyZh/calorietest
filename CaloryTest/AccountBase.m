//
//  Account.m
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 18.06.15.
//
//

#import "AccountBase.h"
#import "Meal.h"


@implementation AccountBase

@dynamic active;
@dynamic calorie;
@dynamic username;
@dynamic data;

@end
