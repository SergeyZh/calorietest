//
//  CalendarEntity.m
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 20.06.15.
//
//

#import "CalendarEntity.h"


@implementation CalendarEntity

@dynamic date;
@dynamic timeOfDay;

@end
