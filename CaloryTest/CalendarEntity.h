//
//  CalendarEntity.h
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 20.06.15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "RemoteObject.h"


@interface CalendarEntity : RemoteObject

@property (nonatomic, retain) NSDate * date;
@property (nonatomic, retain) NSDate * timeOfDay;

@end
