//
//  Account.h
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 15.06.15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>
#import "AccountBase.h"

@class Meal;

@interface Account : AccountBase

@property (nonatomic, assign) NSString *password;

@end

@interface Account (CoreDataGeneratedAccessors)

- (void)addDataObject:(Meal *)value;
- (void)removeDataObject:(Meal *)value;
- (void)addData:(NSSet *)values;
- (void)removeData:(NSSet *)values;

@end
