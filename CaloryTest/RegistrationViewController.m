//
//  RegistrationViewController.m
//  CaloryTest
//
//  Created by Sergey Zhitnikov on 13.06.15.
//
//

#import "RegistrationViewController.h"
#import <Parse/Parse.h>
#import <CoreData/CoreData.h>
#import "Account.h"
#import "AppDelegate.h"

@interface RegistrationViewController ()

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation RegistrationViewController

- (void)viewDidLoad {
    [super viewDidLoad];
}

// Hide keyboard on touch outside
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (IBAction)onRegistration {
    // Create Parse user
    PFUser *user = [PFUser user];
    user.username = self.loginTextField.text;
    user.password = self.passwordTextField.text;
    
    // Save user data to Core Data
    Account *account = [NSEntityDescription insertNewObjectForEntityForName:@"Account"
                                                     inManagedObjectContext:_managedObjectContext];
    
    account.username = user.username;
    account.password = user.password;
    
    [user signUpInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        if (!error) {
            if (![_managedObjectContext save:&error]) {
               NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            }
            [self.navigationController popViewControllerAnimated:YES];
        } else {
            NSString *errorString = [error userInfo][@"error"];
            UIAlertView *registrationAlert = [[UIAlertView alloc] initWithTitle:@"Ошибка регистрации"
                                                                        message:errorString
                                                                       delegate:nil
                                                              cancelButtonTitle:@"ОК"
                                                              otherButtonTitles:nil];
            [registrationAlert show];
        }
    }];
}

#pragma mark - Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField
{
    if (_loginTextField == textField) {
        [_passwordTextField becomeFirstResponder];
    }
    else if (_passwordTextField == textField)
    {
        [self onRegistration];
        [textField resignFirstResponder];
    }
    
    return YES;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
