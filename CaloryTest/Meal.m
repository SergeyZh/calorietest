//
//  Meal.m
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 19.06.15.
//
//

#import "Meal.h"
#import "Account.h"


@implementation Meal

@dynamic calorie;
@dynamic title;
@dynamic account;

@end
