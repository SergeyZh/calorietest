//
//  RegistrationViewController.h
//  CaloryTest
//
//  Created by Sergey Zhitnikov on 13.06.15.
//
//

#import <UIKit/UIKit.h>

@interface RegistrationViewController : UIViewController <UITextFieldDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;

@end
