//
//  FilterViewController.m
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 18.06.15.
//
//

#import "FilterViewController.h"
#import "MainViewController.h"
#import "NSDate+DateCutter.h"

@interface FilterViewController ()

@property (weak, nonatomic) IBOutlet UIDatePicker *fromPicker;
@property (weak, nonatomic) IBOutlet UIDatePicker *toPicker;
@property (weak, nonatomic) IBOutlet UIBarButtonItem *submitBarButton;

@end

@implementation FilterViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    _fromPicker.date = self.fromDate;
    _toPicker.date = self.toDate;
    
    _fromPicker.maximumDate = [NSDate date];
}

-(UIBarPosition)positionForBar:(id<UIBarPositioning>)bar {
    return UIBarPositionTopAttached;
}

- (IBAction)cancel:(UIBarButtonItem *)sender {
    [self.delegate clearFilter];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)submit:(UIBarButtonItem *)sender {
    [self.delegate filterFrom:_fromPicker.date to:_toPicker.date];
    [self dismissViewControllerAnimated:YES completion:nil];
}
/*
 * Validate date range
 */
- (IBAction)pickersValueChanged:(UIDatePicker *)sender {
    if ([[_fromPicker.date day] compare:[_toPicker.date day]] == NSOrderedDescending ||
        [[_fromPicker.date timeOfDay] compare:[_toPicker.date timeOfDay]] == NSOrderedDescending)
    {
        [_submitBarButton setEnabled:NO];
    }
    else
    {
        [_submitBarButton setEnabled:YES];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
