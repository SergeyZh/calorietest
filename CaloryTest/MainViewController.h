//
//  CTMainViewController.h
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 14.06.15.
//
//

#import <UIKit/UIKit.h>
#import "DetailViewController.h"
#import "FilterViewController.h"

@interface MainViewController : UIViewController <UITableViewDataSource, UITableViewDelegate, NSFetchedResultsControllerDelegate, DetailViewControllerDelegate, UITextFieldDelegate, FilterDelegate>

@property (nonatomic, strong) NSManagedObjectContext *managedObjectContext;
@property (nonatomic, strong) NSDate *fromDate;
@property (nonatomic, strong) NSDate *toDate;

@end
