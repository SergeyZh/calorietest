//
//  CalorieTableViewCell.m
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 14.06.15.
//
//

#import "CalorieTableViewCell.h"

@implementation CalorieTableViewCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setDatasource:(Meal *)datasource
{
    if (datasource != self.datasource) {
        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"HH:mm"];
        
        self.timeLabel.text = [formatter stringFromDate:datasource.timeOfDay];
        self.titleLabel.text = datasource.title;
        self.calorieLabel.text = [NSString stringWithFormat:@"%@ %@", [datasource.calorie stringValue], @"КК"];
    }
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
