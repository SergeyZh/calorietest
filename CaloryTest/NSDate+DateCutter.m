//
//  NSDate+DateCutter.m
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 19.06.15.
//
//

#import "NSDate+DateCutter.h"

@implementation NSDate (DateCutter)

- (NSDate *)timeOfDay
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *timeComponents = [calendar components:NSHourCalendarUnit|NSMinuteCalendarUnit
                                                   fromDate:[NSDate dateWithTimeInterval:0
                                                                               sinceDate:self]];
    
    [timeComponents setMonth:1];
    [timeComponents setDay:0];
    [timeComponents setYear:0];
    
    return [calendar dateFromComponents:timeComponents];
}

- (NSDate *)day
{
    NSCalendar *calendar = [NSCalendar currentCalendar];
    NSDateComponents *dateComponents = [calendar components:NSYearCalendarUnit|NSMonthCalendarUnit|NSDayCalendarUnit
                                                   fromDate:self];
    
    return [calendar dateFromComponents:dateComponents];
}

+ (NSDate *)combineDate:(NSDate *)date withTime:(NSDate *)time {
    
    NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSGregorianCalendar];
    
    unsigned unitFlagsDate = NSYearCalendarUnit | NSMonthCalendarUnit |  NSDayCalendarUnit;
    NSDateComponents *dateComponents = [gregorian components:unitFlagsDate fromDate:date];
    unsigned unitFlagsTime = NSHourCalendarUnit | NSMinuteCalendarUnit |  NSSecondCalendarUnit;
    NSDateComponents *timeComponents = [gregorian components:unitFlagsTime fromDate:time];
    
    [dateComponents setSecond:[timeComponents second]];
    [dateComponents setHour:[timeComponents hour]];
    [dateComponents setMinute:[timeComponents minute]];
    
    NSDate *combDate = [gregorian dateFromComponents:dateComponents];
    
    return combDate;
}

@end
