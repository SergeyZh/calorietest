//
//  CTMainViewController.m
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 14.06.15.
//
//

#import "MainViewController.h"
#import "AppDelegate.h"
#import "CalorieTableViewCell.h"
#import "Meal.h"
#import <Parse/Parse.h>
#import "LoginViewController.h"
#import "Account.h"
#import "NSDate+DateCutter.h"

#define DETAIL_SEGUE @"Detail"
#define ADD_SEGUE @"Add"
#define FILTER_SEGUE @"Filter"

@interface MainViewController ()

@property (nonatomic, strong) Account *currentUser;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *maxCalorieTextField;
/*
 *  Use separate FRC (and single table view) for usual fetch and filtering
 */
@property (nonatomic, strong) NSFetchedResultsController *fetchedResultsController;
@property (nonatomic, copy) NSFetchedResultsController *filterResultController;

@end

@implementation MainViewController

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    NSError *error;
    if (![[self fetchedResultsController] performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillShow:)
                                                 name:UIKeyboardWillShowNotification
                                               object:nil];
    
    [[NSNotificationCenter defaultCenter] addObserver:self
                                             selector:@selector(keyboardWillHide:)
                                                 name:UIKeyboardWillHideNotification
                                               object:nil];
}

- (void)viewDidAppear:(BOOL)animated
{
    if (!_filterResultController)
    {
        UIBarButtonItem * addButton = [[UIBarButtonItem alloc]
                                       initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                       target:self
                                       action:@selector(onAdd)];
    
        [self changeBarItem:addButton];
    }
}

- (void)dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillShowNotification object:nil];
    [[NSNotificationCenter defaultCenter] removeObserver:self name:UIKeyboardWillHideNotification object:nil];
}

- (void)calorieSetup
{
    _maxCalorieTextField.text = [NSString stringWithFormat:@"Мне хватит %@ калорий в день", self.currentUser.calorie];
    [_tableView reloadData];
    NSError *error;
    if (![self.managedObjectContext save:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
}

#pragma mark Table View Datasource
/*
 * Remove cell separator offset
 */
-(void)viewDidLayoutSubviews
{
    [super viewDidLayoutSubviews];
    
    if ([self.tableView respondsToSelector:@selector(setSeparatorInset:)]) {
        [self.tableView setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([self.tableView respondsToSelector:@selector(setLayoutMargins:)]) {
        [self.tableView setLayoutMargins:UIEdgeInsetsZero];
    }
}

-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    if ([cell respondsToSelector:@selector(setSeparatorInset:)]) {
        [cell setSeparatorInset:UIEdgeInsetsZero];
    }
    
    if ([cell respondsToSelector:@selector(setLayoutMargins:)]) {
        [cell setLayoutMargins:UIEdgeInsetsZero];
    }
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    
    id <NSFetchedResultsSectionInfo> sectionInfo = _filterResultController ? [[self.filterResultController sections] objectAtIndex:section]  : [[self.fetchedResultsController sections] objectAtIndex:section];
    return [sectionInfo numberOfObjects];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return _filterResultController ? [[self.filterResultController sections] count] : [[self.fetchedResultsController sections] count];
}

- (UIView *)tableView:(UITableView *)tableView viewForFooterInSection:(NSInteger)section
{
    return nil;
}

- (CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section
{
    return 30;
}

- (UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section
{
    UIView *view = [[UIView alloc] initWithFrame:CGRectMake(0, 0, tableView.frame.size.width, tableView.sectionHeaderHeight)];
    view.backgroundColor = [UIColor colorWithRed:204.0f/255.0f green:255.0f/255.0f blue:102.0f/255.0f alpha:1.0f];
    
    id <NSFetchedResultsSectionInfo> sectionInfo = [[self.fetchedResultsController sections] objectAtIndex:section];
    
    int resultCalorie = 0;
    for (int i = 0; i < [sectionInfo numberOfObjects]; i++)
    {
        resultCalorie += [((Meal *)sectionInfo.objects[i]).calorie intValue];
    }
    
    UIColor *darkGreen = [UIColor colorWithRed:0.0f/255.0f green:128.0f/255.0f blue:64.0f/255.0f alpha:1.0f];
    
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"dd/MM/yyyy"];
    
    UILabel *dateLabel = [[UILabel alloc] initWithFrame:CGRectMake(10, 5, 150, tableView.sectionHeaderHeight)];
    dateLabel.text = [formatter stringFromDate:((Meal *)sectionInfo.objects[0]).date];
    dateLabel.textColor = darkGreen;
    
    UILabel *calorieLabel = [[UILabel alloc] initWithFrame:CGRectMake(tableView.frame.size.width - 110, 5, 100, tableView.sectionHeaderHeight)];
    calorieLabel.text = [NSString stringWithFormat:@"%u/%@", resultCalorie, _currentUser.calorie];
    calorieLabel.textAlignment = NSTextAlignmentRight;
    calorieLabel.textColor = resultCalorie > [_currentUser.calorie intValue] ? [UIColor redColor] : darkGreen;
    
    [view addSubview:dateLabel];
    [view addSubview:calorieLabel];
    
    return view;
}

- (void)configureCell:(CalorieTableViewCell *)cell atIndexPath:(NSIndexPath *)indexPath {
    Meal *meal = _filterResultController ? [self.filterResultController objectAtIndexPath:indexPath] : [self.fetchedResultsController objectAtIndexPath:indexPath];
    cell.datasource = meal;
    cell.userInteractionEnabled = _filterResultController ? NO : YES;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSString *reuseIdentifier = @"CalorieCell";
    CalorieTableViewCell *cell = [_tableView dequeueReusableCellWithIdentifier:reuseIdentifier];
    
    [self configureCell:cell atIndexPath:indexPath];
    
    return cell;
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (_filterResultController)
    {
        return NO;
    }
    else
    {
        return YES;
    }
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    if (editingStyle == UITableViewCellEditingStyleDelete) {
        
        // Delete the managed object
        NSManagedObjectContext *context = [self.fetchedResultsController managedObjectContext];
        
        PFObject *deletingObject = [PFObject objectWithClassName:@"Meal"];
        deletingObject.objectId = ((Meal *)[self.fetchedResultsController objectAtIndexPath:indexPath]).objectId;
        [deletingObject deleteInBackground];
        
        [context deleteObject:[self.fetchedResultsController objectAtIndexPath:indexPath]];
        
        NSError *error;
        if (![context save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    [self performSegueWithIdentifier:DETAIL_SEGUE sender:self];
}

#pragma mark - Fetched results controller

/*
 Returns the fetched results controller. Creates and configures the controller if necessary.
 */
- (NSFetchedResultsController *)fetchedResultsController {
    
    if (_fetchedResultsController != nil) {
        return _fetchedResultsController;
    }
    
    self.managedObjectContext = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).managedObjectContext;
    
    // Get our current user
    if (self.currentUser == nil)
    {
        NSFetchRequest *userRequest = [[NSFetchRequest alloc] init];
        NSEntityDescription *accountEntity = [NSEntityDescription entityForName:@"Account"
                                                         inManagedObjectContext:self.managedObjectContext];
        [userRequest setEntity:accountEntity];
        // PFUser doesn't save username and password
        NSPredicate *currentUserPredicate = [NSPredicate predicateWithFormat:@"active == YES"];
        [userRequest setPredicate:currentUserPredicate];
        
        NSError* error;
        NSArray *result = [self.managedObjectContext executeFetchRequest:userRequest error:&error];
        
        if (error != nil)
        {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
            return nil;
        }
        
        self.currentUser = (Account *)result[0];
        
        [self calorieSetup];
    }
    
    // Get user calendary
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *mealEntity = [NSEntityDescription entityForName:@"Meal"
                                                  inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:mealEntity];
    
    NSSortDescriptor *timeDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeOfDay" ascending:NO];
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    NSArray *sortDescriptors = @[dateDescriptor, timeDescriptor];
    [fetchRequest setSortDescriptors:sortDescriptors];

    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"account.username == %@", self.currentUser.username];
    [fetchRequest setPredicate:predicate];

    _fetchedResultsController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"date" cacheName:nil];
    _fetchedResultsController.delegate = self;
    
    return _fetchedResultsController;
}
/*
 * Returns alternative FRC for filtered calendary
 */
- (NSFetchedResultsController *)filterResultController
{
    if (_filterResultController != nil)
    {
        return _filterResultController;
    }
    
    NSFetchRequest *fetchRequest = [[NSFetchRequest alloc] init];
    NSEntityDescription *mealEntity = [NSEntityDescription entityForName:@"Meal"
                                                  inManagedObjectContext:self.managedObjectContext];
    [fetchRequest setEntity:mealEntity];
    
    NSSortDescriptor *timeDescriptor = [[NSSortDescriptor alloc] initWithKey:@"timeOfDay" ascending:NO];
    NSSortDescriptor *dateDescriptor = [[NSSortDescriptor alloc] initWithKey:@"date" ascending:NO];
    NSArray *sortDescriptors = @[dateDescriptor, timeDescriptor];
    [fetchRequest setSortDescriptors:sortDescriptors];
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"(account.username == %@) && ((date >=%@) && (date <= %@) && (timeOfDay >=%@) && (timeOfDay <= %@))"
                              , self.currentUser.username
                              , [self.fromDate day]
                              , [self.toDate day]
                              , [self.fromDate timeOfDay]
                              , [self.toDate timeOfDay]];
    
    [fetchRequest setPredicate:predicate];
    
    _filterResultController = [[NSFetchedResultsController alloc] initWithFetchRequest:fetchRequest managedObjectContext:self.managedObjectContext sectionNameKeyPath:@"date" cacheName:nil];
    _filterResultController.delegate = self;
    
    return _filterResultController;
}

/*
 * NSFetchedResultsController delegate methods to respond to additions, removals and so on.
 */
- (void)controllerWillChangeContent:(NSFetchedResultsController *)controller {
    
    // The fetch controller is about to start sending change notifications, so prepare the table view for updates.
    [self.tableView beginUpdates];
}

- (void)controller:(NSFetchedResultsController *)controller didChangeObject:(id)anObject atIndexPath:(NSIndexPath *)indexPath forChangeType:(NSFetchedResultsChangeType)type newIndexPath:(NSIndexPath *)newIndexPath {
    
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [_tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            if (![_fetchedResultsController.sections count])
            {
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
            }
            break;
            
        case NSFetchedResultsChangeDelete:
            [_tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            if ([_fetchedResultsController.sections count] &&
                [_tableView numberOfRowsInSection:indexPath.section] > 1)
            {
                [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationFade];
            }
            break;
            
        case NSFetchedResultsChangeUpdate:
            [self configureCell:(CalorieTableViewCell*)[_tableView cellForRowAtIndexPath:indexPath] atIndexPath:indexPath];
            [self.tableView reloadSections:[NSIndexSet indexSetWithIndex:indexPath.section] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
            
        case NSFetchedResultsChangeMove:
            [_tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [_tableView insertRowsAtIndexPaths:@[newIndexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            break;
    }
}

- (void)controller:(NSFetchedResultsController *)controller didChangeSection:(id <NSFetchedResultsSectionInfo>)sectionInfo atIndex:(NSUInteger)sectionIndex forChangeType:(NSFetchedResultsChangeType)type
{
    switch(type) {
            
        case NSFetchedResultsChangeInsert:
            [self.tableView insertSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeDelete:
            [self.tableView deleteSections:[NSIndexSet indexSetWithIndex:sectionIndex] withRowAnimation:UITableViewRowAnimationFade];
            break;
            
        case NSFetchedResultsChangeUpdate:
        case NSFetchedResultsChangeMove:
            break;
    }
}

- (void)controllerDidChangeContent:(NSFetchedResultsController *)controller {
    
    // The fetch controller has sent all current change notifications, so tell the table view to process all updates.
    [self.tableView endUpdates];
}

#pragma mark - Bar Handlers

- (void)changeBarItem:(UIBarButtonItem *)item
{
    self.navigationItem.rightBarButtonItem = item;
}

- (IBAction)onAdd {
    [self performSegueWithIdentifier:ADD_SEGUE sender:self];
}

- (void)submitDiet {
    UIBarButtonItem * addButton = [[UIBarButtonItem alloc]
                                   initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                   target:self
                                   action:@selector(onAdd)];
    
    [self changeBarItem:addButton];
    
    [_maxCalorieTextField resignFirstResponder];
}
/*
 * Set current user as inactive and display Login View
 */
- (IBAction)logout {
    [PFUser logOut];
    
    self.currentUser.active = @0;
    NSError *error;
    if(![self.managedObjectContext save:&error])
    {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    self.currentUser = nil;
    
    AppDelegate *appDelegateTemp = [[UIApplication sharedApplication]delegate];
    
    UINavigationController* rootController = [[UIStoryboard storyboardWithName:@"Main" bundle:[NSBundle mainBundle]] instantiateViewControllerWithIdentifier:@"LoginViewController"];
    
    appDelegateTemp.window.rootViewController = rootController;
    
    _tableView = nil;
    
    [self.presentingViewController dismissViewControllerAnimated:NO completion:nil];
}

#pragma mark - Detail Delagate

- (void)saveEntity:(DetailViewController *)controller add:(BOOL)add
{
    NSError *error;
    if (add) {
        NSManagedObjectContext *addingManagedObjectContext = [controller managedObjectContext];
        
        // Create Parse object
        PFObject *pfMeal = [PFObject objectWithClassName:@"Meal"];
        __block __weak Meal *cdMeal = controller.meal;
        pfMeal[@"title"] = cdMeal.title;
        pfMeal[@"calorie"] = cdMeal.calorie;
        pfMeal[@"date"] = cdMeal.date;
        pfMeal[@"timeOfDay"] = cdMeal.timeOfDay;
        
        PFUser *user = [PFUser currentUser];
        PFRelation *relation = [pfMeal relationForKey:@"account"];
        [relation addObject:user];
            
        [pfMeal saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
            if (succeeded) {
                // The object has been saved.
                // Save Parse objectID for future updates
                cdMeal.objectId = pfMeal.objectId;
                cdMeal.updatedAt = pfMeal.updatedAt;
                cdMeal.createdAt = pfMeal.createdAt;
                
                if (![addingManagedObjectContext save:&error]) {
                    NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
                }
            } else {
                // There was a problem, check error.description
            }
        }];
        
        // Call context save for immediate update
        if (![addingManagedObjectContext save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
    else
    {
        // Update Parse object with saved objectID
        PFObject *pfMeal = [PFObject objectWithClassName:@"Meal"];
        Meal *cdMeal = controller.meal;
        pfMeal[@"title"] = cdMeal.title;
        pfMeal[@"calorie"] = cdMeal.calorie;
        pfMeal[@"date"] = cdMeal.date;
        pfMeal[@"timeOfDay"] = cdMeal.timeOfDay;
        pfMeal.objectId = cdMeal.objectId;
        
        [pfMeal saveInBackground];
        
        if (![[self.fetchedResultsController managedObjectContext] save:&error]) {
            NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
        }
    }
}

#pragma mark - Filter Delegate

- (void)clearFilter
{
    self.fromDate = nil;
    self.toDate = nil;
    
    self.filterResultController = nil;
    
    [_tableView reloadData];
    
    UIBarButtonItem * addButton = [[UIBarButtonItem alloc]
                                   initWithBarButtonSystemItem:UIBarButtonSystemItemAdd
                                   target:self
                                   action:@selector(onAdd)];
    
    [self changeBarItem:addButton];
}

- (void)filterFrom:(NSDate *)from to:(NSDate *)to
{
    self.fromDate = from;
    self.toDate = to;
    self.filterResultController = nil;
    
    NSError *error;
    if (![[self filterResultController] performFetch:&error]) {
        NSLog(@"Unresolved error %@, %@", error, [error userInfo]);
    }
    
    self.navigationItem.rightBarButtonItem = nil;

    [_tableView reloadData];
    
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                    style:UIBarButtonItemStyleDone
                                                                   target:self
                                                                   action:@selector(clearFilter)];
    
    [self changeBarItem:doneButton];
}

#pragma mark - Navigation
/*
 * Handle adding and editing segue
 */
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:ADD_SEGUE]) {
        DetailViewController *detailViewController = (DetailViewController *)[segue destinationViewController];
        detailViewController.delegate = self;
        
        // Create a new managed object context for the new meal; set its parent to the fetched results controller's context.
        NSManagedObjectContext *addingContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSPrivateQueueConcurrencyType];
        [addingContext setParentContext:[self.fetchedResultsController managedObjectContext]];
        
        Meal *newMeal = (Meal *)[NSEntityDescription insertNewObjectForEntityForName:@"Meal" inManagedObjectContext:addingContext];
        detailViewController.meal = newMeal;
        detailViewController.managedObjectContext = addingContext;
        detailViewController.isEdit = NO;
        detailViewController.userID = [_currentUser objectID];
    }
    
    if ([[segue identifier] isEqualToString:DETAIL_SEGUE]) {
        NSIndexPath *indexPath = [self.tableView indexPathForSelectedRow];
        Meal *selectedMeal = (Meal *)[[self fetchedResultsController] objectAtIndexPath:indexPath];
        
        // Pass the selected meal to the new view controller.
        DetailViewController *detailViewController = (DetailViewController *)[segue destinationViewController];
        detailViewController.meal = selectedMeal;
        detailViewController.delegate = self;
        detailViewController.managedObjectContext = self.managedObjectContext;
        detailViewController.isEdit = YES;
        detailViewController.userID = [_currentUser objectID];
    }
    
    if ([[segue identifier] isEqualToString:FILTER_SEGUE]) {
        FilterViewController *filterView = (FilterViewController *)[segue destinationViewController];
        filterView.delegate = self;
        filterView.fromDate = _fromDate ? self.fromDate : [[NSDate date] day];
        filterView.toDate = _toDate ? self.toDate : [NSDate date];
    }
}

#pragma mark - Text Field Delegate

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    UIBarButtonItem * doneButton = [[UIBarButtonItem alloc] initWithTitle:@"Готово"
                                                                    style:UIBarButtonItemStyleDone
                                                                   target:self
                                                                   action:@selector(submitDiet)];
    
    [self changeBarItem:doneButton];
}

- (void)textFieldDidEndEditing:(UITextField *)textField
{
    // Set new maximum for a day
    if (textField == _maxCalorieTextField && textField.text.length)
    {
        self.currentUser.calorie = [NSNumber numberWithInteger:[textField.text integerValue]];
    
    }
    [self calorieSetup];
}
/*
 * Placing our text field above any keyboard
 */
- (void)keyboardWillShow:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGRect kbRect = [info [UIKeyboardFrameEndUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    CGFloat kbDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    [self animateView:_maxCalorieTextField.superview height:kbRect.size.height duration:kbDuration up:YES];
}

- (void)keyboardWillHide:(NSNotification*)notification {
    NSDictionary *info = [notification userInfo];
    CGRect kbRect = [info [UIKeyboardFrameEndUserInfoKey] CGRectValue];
    kbRect = [self.view convertRect:kbRect fromView:nil];
    CGFloat kbDuration = [[info objectForKey:UIKeyboardAnimationDurationUserInfoKey] floatValue];
    
    [self animateView:_maxCalorieTextField.superview height:kbRect.size.height duration:kbDuration up:NO];
}

-(void)animateView:(UIView*)animatedView height:(CGFloat)heigth duration:(CGFloat)duration up:(BOOL)up
{
    int movementDistance = heigth;
    float movementDuration = duration;
    
    int movement = (up ? -movementDistance : movementDistance);
    
    [UIView beginAnimations: @"animateTextField" context: nil];
    [UIView setAnimationBeginsFromCurrentState: YES];
    [UIView setAnimationDuration: movementDuration];
    animatedView.frame = CGRectOffset(animatedView.frame, 0, movement);
    [UIView commitAnimations];
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
