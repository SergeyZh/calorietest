//
//  Account.h
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 18.06.15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>

@class Meal;

@interface AccountBase : NSManagedObject

@property (nonatomic, retain) NSNumber * active;
@property (nonatomic, retain) NSNumber * calorie;
@property (nonatomic, retain) NSString * username;
@property (nonatomic, retain) NSSet *data;
@end

@interface AccountBase (CoreDataGeneratedAccessors)

- (void)addDataObject:(Meal *)value;
- (void)removeDataObject:(Meal *)value;
- (void)addData:(NSSet *)values;
- (void)removeData:(NSSet *)values;

@end
