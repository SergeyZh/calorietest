//
//  RemoteObject.h
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 20.06.15.
//
//

#import <Foundation/Foundation.h>
#import <CoreData/CoreData.h>


@interface RemoteObject : NSManagedObject

@property (nonatomic, retain) NSDate * createdAt;
@property (nonatomic, retain) NSDate * updatedAt;
@property (nonatomic, retain) NSString * objectId;

@end
