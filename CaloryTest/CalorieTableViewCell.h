//
//  CalorieTableViewCell.h
//  CalorieTest
//
//  Created by Sergey Zhitnikov on 14.06.15.
//
//

#import <UIKit/UIKit.h>
#import "Meal.h"

@interface CalorieTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *timeLabel;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UILabel *calorieLabel;
@property (strong, nonatomic) Meal *datasource;

@end
