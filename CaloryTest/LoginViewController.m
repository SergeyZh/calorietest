//
//  CTLoginViewController.m
//  CaloryTest
//
//  Created by Sergey Zhitnikov on 13.06.15.
//
//

#import "LoginViewController.h"
#import "RegistrationViewController.h"
#import "Account.h"
#import "AppDelegate.h"
#import <Parse/Parse.h>

#define LOGIN_SEGUE @"Login"
#define REGISTRATION_SEGUE @"Registration"

@interface LoginViewController ()

@property (weak, nonatomic) IBOutlet UITextField *loginTextField;
@property (weak, nonatomic) IBOutlet UITextField *passwordTextField;

@end

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.managedObjectContext = ((AppDelegate *)[[UIApplication sharedApplication] delegate]).managedObjectContext;
}

// Hide keyboard on touch outside
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event {
    [self.view endEditing:YES];
}

- (IBAction)onLogin
{
    // Creating temporary context for current user update
    NSManagedObjectContext *tempContext = [[NSManagedObjectContext alloc]
                                           initWithConcurrencyType:NSPrivateQueueConcurrencyType];
    [tempContext setParentContext:_managedObjectContext];
    
    [PFUser logInWithUsernameInBackground:self.loginTextField.text password:self.passwordTextField.text
                                    block:^(PFUser *user, NSError *error) {
                                        if (user) {
                                            // Activate account
                                            Account *account;
                                            
                                            NSEntityDescription *accountEntity = [NSEntityDescription entityForName:@"Account"
                                                                                             inManagedObjectContext:tempContext];
                                            
                                            NSFetchRequest *userRequest = [[NSFetchRequest alloc] init];
                                            [userRequest setEntity:accountEntity];
                                            
                                            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"username == %@", _loginTextField.text];
                                            [userRequest setPredicate:predicate];
                                            
                                            NSError *error;
                                            NSArray *result = [self.managedObjectContext executeFetchRequest:userRequest error:&error];
                                            
                                            if ([result count])
                                            {
                                                account = (Account *)result[0];
                                                account.active = @1;
                                            }
                                            // PFUser is existing but not saved on current device
                                            else
                                            {
                                                account = (Account *)[NSEntityDescription insertNewObjectForEntityForName:@"Account"
                                                                                                   inManagedObjectContext:_managedObjectContext];
                                                
                                                account.username = self.loginTextField.text;
                                                account.password = self.passwordTextField.text;
                                                account.active = @1;
                                            }
                                            
                                            if (![tempContext save:&error]) {
                                                NSLog(@"%@\n%@", error.localizedDescription, error.localizedRecoverySuggestion);
                                                UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:self cancelButtonTitle:@"ОК" otherButtonTitles: nil];
                                                [alert show];
                                            }
                                            
                                            [self performSegueWithIdentifier:LOGIN_SEGUE sender:self];
                                        } else {
                                            // The login failed. Check error to see why.
                                            /*
                                             * Offline login should be implemented here
                                             */
                                            Account *account;
                                            
                                            NSEntityDescription *accountEntity = [NSEntityDescription entityForName:@"Account"
                                                                                             inManagedObjectContext:tempContext];
                                            
                                            NSFetchRequest *userRequest = [[NSFetchRequest alloc] init];
                                            [userRequest setEntity:accountEntity];
                                            
                                            NSPredicate *predicate = [NSPredicate predicateWithFormat:@"username == %@", _loginTextField.text];
                                            [userRequest setPredicate:predicate];
                                            
                                            NSError *error;
                                            NSArray *result = [self.managedObjectContext executeFetchRequest:userRequest error:&error];
                                            
                                            if ([result count])
                                            {
                                                account = (Account *)result[0];
                                                if (account.password == _passwordTextField.text)
                                                {
                                                    account.active = @1;
                                                    
                                                    if (![tempContext save:&error]) {
                                                        NSLog(@"%@\n%@", error.localizedDescription, error.localizedRecoverySuggestion);
                                                        UIAlertView *errorAlert = [[UIAlertView alloc] initWithTitle:nil message:error.localizedDescription delegate:self cancelButtonTitle:@"ОК" otherButtonTitles: nil];
                                                        [errorAlert show];
                                                    }
                                                }
                                                else
                                                {
                                                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:nil message:@"Проверьте введенный пароль или зарегистрируйтесь" delegate:self cancelButtonTitle:@"ОК" otherButtonTitles: nil];
                                                    [alert show];
                                                }
                                            }
                                        }
                                    }];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

#pragma mark - Text Field Delegate

- (BOOL)textFieldShouldReturn:(UITextField *)textField {
    if (_loginTextField == textField) {
        [_passwordTextField becomeFirstResponder];
    }
    else if (_passwordTextField == textField)
    {
        [self onLogin];
        [textField resignFirstResponder];
    }
    
    return YES;
}

#pragma mark - Navigation

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier]  isEqual: REGISTRATION_SEGUE])
    {
        RegistrationViewController *registrationView = [segue destinationViewController];
        NSManagedObjectContext *regContext = [[NSManagedObjectContext alloc] initWithConcurrencyType:NSMainQueueConcurrencyType];
        [regContext setParentContext:self.managedObjectContext];
        registrationView.managedObjectContext = regContext;
    }
}

@end
